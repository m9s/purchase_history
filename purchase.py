# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
import copy
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.transaction import Transaction


class Purchase(ModelWorkflow, ModelSQL, ModelView):
    _name = 'purchase.purchase'

    confirm_date = fields.DateTime('Confirm Date')

    def __init__(self):
        super(Purchase, self).__init__()
        self.party = copy.copy(self.party)
        self.party.datetime_field = 'confirm_date'
        if 'confirm_date' not in self.party.depends:
            self.party.depends = copy.copy(self.party.depends)
            self.party.depends.append('confirm_date')
        self.invoice_address = copy.copy(self.invoice_address)
        self.invoice_address.datetime_field = 'confirm_date'
        if 'confirm_date' not in self.invoice_address.depends:
            self.invoice_address.depends = copy.copy(
                self.invoice_address.depends)
            self.invoice_address.depends.append('confirm_date')
        self.payment_term = copy.copy(self.payment_term)
        self.payment_term.datetime_field = 'confirm_date'
        if 'confirm_date' not in self.payment_term.depends:
            self.payment_term.depends = copy.copy(
                self.payment_term.depends)
            self.payment_term.depends.append('confirm_date')
        self._reset_columns()

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['confirm_date'] = False
        return super(Purchase, self).copy(ids, default=default)

    def set_confirm_date(self, sale_id):
        self.write(sale_id, {
            'confirm_date': datetime.datetime.now(),
            })
        return True

    def wkf_draft(self, sale):
        super(Purchase, self).wkf_draft(sale)
        self.write(sale.id, {'confirm_date': False})

    def wkf_confirmed(self, sale):
        super(Purchase, self).wkf_confirmed(sale)
        self.set_confirm_date(sale.id)

Purchase()


class PurchaseLine(ModelSQL, ModelView):
    _name = 'purchase.line'

    confirm_date = fields.Function(fields.DateTime('Confirm Date'),
        'get_confirm_date')

    def __init__(self):
        super(PurchaseLine, self).__init__()
        self.product = copy.copy(self.product)
        self.product.datetime_field = 'confirm_date'
        if 'confirm_date' not in self.product.depends:
            self.product.depends.append('confirm_date')
        self._reset_columns()

    def get_confirm_date(self, ids, name):
        res = {}
        for line_id in ids:
            res[line_id] = False
        cursor = Transaction().cursor
        cursor.execute('SELECT l.id AS id, p.confirm_date AS confirm_date '
            'FROM purchase_purchase p '
                'JOIN purchase_line l '
                'ON p.id = l.purchase '
            'WHERE p.id IN ('
                'SELECT purchase FROM purchase_line WHERE id in (%s))' %
            (','.join(map(str, ids)),))
        values = cursor.fetchall()
        for value in values:
            res[value[0]] = value[1] or False
        return res

PurchaseLine()

